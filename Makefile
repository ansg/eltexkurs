server : ./Server/DieWithError.c ./Server/TCPEchoServer-Thread.c
	gcc -o TCPEchoServer-Thread ./Server/TCPEchoServer-Thread.c ./Server/DieWithError.c ./Server/AcceptTCPConnection.c \
	./Server/CreateTCPServerSocket.c ./Server/HandleTCPClientRecv.c ./Server/HandleTCPClientSend.c ./Server/Broadcast.c \
	 ./Server/amessage.pb-c.c ./Server/queue.c ./Server/queue_sync.c -pthread -lprotobuf-c
clean :
	rm TCPEchoServer-Thread
