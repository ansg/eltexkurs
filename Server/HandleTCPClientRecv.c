#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for recv() and send() */
#include <unistd.h>     /* for close() */
#include <string.h>
#include "amessage.pb-c.h"
#include <stdlib.h>

#define MAX_MSG_SIZE 1024

void DieWithError(char *errorMessage);  /* Error handling function */

uint8_t * HandleTCPClientRecv(int clntSocket, size_t *size)
{
    uint8_t *buf; //Buffer to store serialized data
    buf = (uint8_t *)malloc(MAX_MSG_SIZE);
    size_t recvMsgSize = 0;
    //size_t msg_len = 0;
    /* Receive message from client */
    if ((recvMsgSize = recv(clntSocket, buf, MAX_MSG_SIZE, 0)) < 0)
        DieWithError("recv() failed");
    (*size)+=recvMsgSize;
    /* Send received string and receive again until end of transmission */
    while (recvMsgSize != 0)      /* zero indicates end of transmission */
    {
      /*  *//* Echo message back to client *//*
        if (send(clntSocket, echoBuffer, recvMsgSize, 0) != recvMsgSize)
            DieWithError("send() failed");*/

        /* See if there is more data to receive */
        if ((recvMsgSize = recv(clntSocket, buf, MAX_MSG_SIZE, 0)) < 0)
            DieWithError("recv() failed");
        (*size)+=recvMsgSize;
    }

    close(clntSocket);    /* Close client socket */

    return buf;

}

