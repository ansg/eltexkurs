#include "TCPEchoServer.h"  /* TCP echo server includes */
#include <pthread.h>        /* for POSIX threads */
#include "amessage.pb-c.h"
#include "queue.h"
#include "queue_sync.h"

void *ThreadMainRecv(void *threadArgs);            /* Main program of a thread */
void *ThreadMainSend(void *threadArgs);
void *ThreadRecv(void *arg);            /* Main program of a thread */
void *ThreadSend(void *arg);            /* Main program of a thread */
void *ThreadBroadcast(void *arg);            /* Main program of a thread */

/* Structure of arguments to pass to client thread */
struct ThreadArgs {
    int clntSock;                      /* Socket descriptor for client */
};
struct ThreadPort {
    int port;
    int type;
};

#define MAX_MSG_SIZE 1024
#define NUMTHRDS 4

pthread_t threadID[NUMTHRDS];              /* Thread ID from pthread_create() */
pthread_mutex_t mutexList;

int main(int argc, char *argv[]) {
    unsigned short echoServPortRecv, echoServPortSend;     /* Server port */
    struct ThreadPort *threadRecv;   /* Pointer to argument structure for thread */
    struct ThreadPort *threadSend;   /* Pointer to argument structure for thread */

    if (argc != 3)     /* Test for correct number of arguments */
    {
        fprintf(stderr, "Usage:  %s <SERVER PORT RECV> <SERVER PORT SEND>\n", argv[0]);
        exit(1);
    }
    pthread_mutex_init(&mutexList, NULL);
    pthread_mutex_init(&m_SuspendMutexRecv, NULL);
    pthread_mutex_init(&m_SuspendMutexSend, NULL);
    pthread_cond_init(&m_ResumeCondRecv, NULL);
    pthread_cond_init(&m_ResumeCondSend, NULL);
    list_sync.count = 0;
    list_sync.max_size = 3;
    CIRCLEQ_INIT(&head); //init queue

    resumeMeSend();
    resumeMeRecv();
    //add_to_queue_tail(1, 1, "hi");
    echoServPortRecv = atoi(argv[1]);  /* First arg:  local port */
    echoServPortSend = atoi(argv[2]);

    // Create separate memory for client argument
    if ((threadRecv = (struct ThreadPort *) malloc(sizeof(struct ThreadPort)))
        == NULL)
        DieWithError("malloc() failed");
    threadRecv->port = echoServPortRecv;
    threadRecv->type = 1;

    // Create broadcast thread
    if (pthread_create(&threadID[0], NULL, ThreadBroadcast, (void *) threadRecv) != 0)
        DieWithError("pthread_create() failed");
    printf("with thread %ld\n", (long int) threadID[0]);

     //Create client recv thread
    if (pthread_create(&threadID[1], NULL, ThreadSend, (void *) threadRecv) != 0)
        DieWithError("pthread_create() failed");
    printf("with thread %ld\n", (long int) threadID[1]);

    // Create separate memory for client argument
    if ((threadSend = (struct ThreadPort *) malloc(sizeof(struct ThreadPort)))
        == NULL)
        DieWithError("malloc() failed");
    threadSend->port = echoServPortSend;
    threadSend->type = 2;

    // Create broadcast thread
    if (pthread_create(&threadID[2], NULL, ThreadBroadcast, (void *) threadSend) != 0)
        DieWithError("pthread_create() failed");
    printf("with thread %ld\n", (long int) threadID[2]);

     //Create client send thread
    if (pthread_create(&threadID[3], NULL, ThreadRecv, (void *) threadSend) != 0)
        DieWithError("pthread_create() failed");
    printf("with thread %ld\n", (long int) threadID[3]);

    printf("port recv: %d, port send: %d\n", threadRecv->port, threadSend->port);

    pthread_join(threadID[0], NULL);
    pthread_join(threadID[1], NULL);
    pthread_join(threadID[2], NULL);
    pthread_join(threadID[3], NULL);
    pthread_mutex_destroy(&mutexList);
    pthread_mutex_destroy(&m_SuspendMutexRecv);
    pthread_mutex_destroy(&m_SuspendMutexSend);
    pthread_cond_destroy(&m_ResumeCondRecv);
    pthread_cond_destroy(&m_ResumeCondSend);
  /*  while (head.cqh_first != (void *)&head)
        CIRCLEQ_REMOVE(&head, head.cqh_first, entries);*/
    return 0;
}

void *ThreadBroadcast(void *arg){
    int port, type;
    pthread_detach(pthread_self());
    port = ((struct ThreadPort *) arg) -> port;
    type = ((struct ThreadPort *) arg) -> type;
    Broadcast(port, type);
}


void *ThreadRecv(void *arg) {

    int port, servSock, clntSock;                   /* Socket descriptor for client connection */
    pthread_t threadID;
    struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */
    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    /* Extract socket file descriptor from argument */
    port = ((struct ThreadPort *) arg)->port;
    //free(arg);              /* Deallocate memory for argument */
    servSock = CreateTCPServerSocket(port);

    for (;;) /* run forever */
    {
        clntSock = AcceptTCPConnection(servSock);

        /* Create separate memory for client argument */
        if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))
            == NULL)
            DieWithError("malloc() failed");
        threadArgs->clntSock = clntSock;

        /* Create client thread */
        if (pthread_create(&threadID, NULL, ThreadMainRecv, (void *) threadArgs) != 0)
            DieWithError("pthread_create() failed");
        printf("with thread %ld\n", (long int) threadID);
    }
}

void *ThreadMainRecv(void *threadArgs) {
    int clntSock;                   /* Socket descriptor for client connection */
    AMessage *msg;
    uint8_t *buf; //Buffer to store serialized data
    size_t msg_len = 0;
    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    /* Extract socket file descriptor from argument */
    clntSock = ((struct ThreadArgs *) threadArgs)->clntSock;
   // free(threadArgs);              /* Deallocate memory for argument */

    buf = HandleTCPClientRecv(clntSock, &msg_len);
    // Unpack the message using protobuf-c.
    msg = amessage__unpack(NULL, msg_len, buf);
    if (msg == NULL)
    {
        fprintf(stderr, "error unpacking incoming message\n");
        return (NULL);
    }

    printf("Recived: T=%d, Len=%d, STR=%.10s\n", msg->a, msg->b, msg->c);

    pthread_mutex_lock(&mutexList);
    add_to_queue_head(msg->a, msg->b, msg->c);
    list_sync.count++;
    pthread_mutex_unlock(&mutexList);
    printf("list size:%d\n", list_sync.count);
    if(list_sync.count >= list_sync.max_size){
        suspendMeSend();
        printf("queue is full\n");
    }
    if(list_sync.count < list_sync.max_size){
        resumeMeRecv();
    }

   // printf("T=%d, Len=%d, STR=%.10s", np->T, np->len, np->str);
    return (NULL);
}

void *ThreadSend(void *arg) {

    int port, servSock, clntSock;                   /* Socket descriptor for client connection */
    pthread_t threadID;
    struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */
    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    /* Extract socket file descriptor from argument */
    port = ((struct ThreadPort *) arg)->port;
  //  free(arg);              /* Deallocate memory for argument */
    servSock = CreateTCPServerSocket(port);

    for (;;) /* run forever */
    {
        clntSock = AcceptTCPConnection(servSock);

        /* Create separate memory for client argument */
        if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))
            == NULL)
            DieWithError("malloc() failed");
        threadArgs->clntSock = clntSock;

        /* Create client thread */
        if (pthread_create(&threadID, NULL, ThreadMainSend, (void *) threadArgs) != 0)
            DieWithError("pthread_create() failed");
        printf("with thread %ld\n", (long int) threadID);
    }
}

void *ThreadMainSend(void *threadArgs) {
    int clntSock;                   /* Socket descriptor for client connection */

    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    /* Extract socket file descriptor from argument */
    clntSock = ((struct ThreadArgs *) threadArgs)->clntSock;
    //free(threadArgs);              /* Deallocate memory for argument */
    if(list_sync.count == 0){
        suspendMeRecv();
        printf("queue is empty\n");
    }
    if(list_sync.count > 0){
        resumeMeSend();
    }

    // if(list_sync.count != list_sync.max_size)
    //for(i = 0; i< list_sync.max_size - list_sync.count; i++) np = np->entries.cqe_next;
    //printf("pack: t=%d, len=%d\n", np->T, np->len);
    HandleTCPClientSend(clntSock);

    return (NULL);
}
