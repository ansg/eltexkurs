//
// Created by nedelko on 8/18/16.
//

#ifndef KURS_QUEUE_H
#define KURS_QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <string.h>



struct entry {
    int T;
    int len;
    char* str;
    CIRCLEQ_ENTRY(entry) entries;
} *np;

CIRCLEQ_HEAD(circleq, entry) head;

void add_to_queue_head(int T, int len, char* str);
void add_to_queue_tail(int T, int len, char* str);

#endif //KURS_QUEUE_H
