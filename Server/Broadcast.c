#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "queue_sync.h"

void DieWithError(char *errorMessage);  /* External error handling function */

#define BROADCAST_IP "255.255.255.255"
#define SERVERPORT1 4951
#define SERVERPORT2 4952
void SendEverything1(char * IP, unsigned short Port, char * str){
    {
        int sock;                         /* Socket */
        struct sockaddr_in broadcastAddr; /* Broadcast address */
        char *broadcastIP;                /* IP broadcast address */
        unsigned short broadcastPort;     /* Server port */
        char *sendString;                 /* String to broadcast */
        int broadcastPermission;          /* Socket opt to set permission to broadcast */
        unsigned int sendStringLen;       /* Length of string to broadcast */

        broadcastIP = IP;            /* First arg:  broadcast IP address */
        broadcastPort = Port;    /* Second arg:  broadcast port */
        sendString = str;             /* Third arg:  string to broadcast */

        /* Create socket for sending/receiving datagrams */
        if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        /* Set socket to allow broadcast */
        broadcastPermission = 1;
        if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
                       sizeof(broadcastPermission)) < 0)
            DieWithError("setsockopt() failed");

        /* Construct local address structure */
        memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
        broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
        broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
        broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

        sendStringLen = strlen(sendString);  /* Find length of sendString */

        for (;;) /* Run forever */
        {
            checkSuspendSend();
            /* Broadcast sendString in datagram to clients every 3 seconds*/
            if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *)
                    &broadcastAddr, sizeof(broadcastAddr)) != sendStringLen)
                DieWithError("sendto() sent a different number of bytes than expected");

            sleep(3);   /* Avoids flooding the network */

        }
        /* NOT REACHED */
    }

}

void SendEverything2(char * IP, unsigned short Port, char * str){
    {
        int sock;                         /* Socket */
        struct sockaddr_in broadcastAddr; /* Broadcast address */
        char *broadcastIP;                /* IP broadcast address */
        unsigned short broadcastPort;     /* Server port */
        char *sendString;                 /* String to broadcast */
        int broadcastPermission;          /* Socket opt to set permission to broadcast */
        unsigned int sendStringLen;       /* Length of string to broadcast */

        broadcastIP = IP;            /* First arg:  broadcast IP address */
        broadcastPort = Port;    /* Second arg:  broadcast port */
        sendString = str;             /* Third arg:  string to broadcast */

        /* Create socket for sending/receiving datagrams */
        if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        /* Set socket to allow broadcast */
        broadcastPermission = 1;
        if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
                       sizeof(broadcastPermission)) < 0)
            DieWithError("setsockopt() failed");

        /* Construct local address structure */
        memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
        broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
        broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
        broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

        sendStringLen = strlen(sendString);  /* Find length of sendString */

        for (;;) /* Run forever */
        {
            checkSuspendRecv();
            /* Broadcast sendString in datagram to clients every 3 seconds*/
            if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *)
                    &broadcastAddr, sizeof(broadcastAddr)) != sendStringLen)
                DieWithError("sendto() sent a different number of bytes than expected");

            sleep(3);   /* Avoids flooding the network */

        }
        /* NOT REACHED */
    }

}

int Broadcast(int port, int type) {
    int fd;
 	char interface[24];
    struct ifreq ifr;
    char *IP, portsnd[24];

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET; //get IP
    strncpy(ifr.ifr_name, "lo", IFNAMSIZ-1); // IP attached eth1
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    IP = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
    strcat(IP, ":");
    sprintf(portsnd, "%d", port);
    strcat(IP, portsnd);
    
    if(type == 1)
        SendEverything2(BROADCAST_IP, SERVERPORT1, IP);
    else
        SendEverything1(BROADCAST_IP, SERVERPORT2, IP);
    return 0;
}

