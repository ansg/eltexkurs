#include "queue_sync.h"
#include <stdio.h>

void suspendMeSend()
{ // tell the thread to suspend
    pthread_mutex_lock(&m_SuspendMutexSend);
    m_SuspendFlagSend = 1;
    pthread_mutex_unlock(&m_SuspendMutexSend);
}
void resumeMeSend()
{ // tell the thread to resume
    pthread_mutex_lock(&m_SuspendMutexSend);
    m_SuspendFlagSend = 0;
    pthread_cond_broadcast(&m_ResumeCondSend);
    pthread_mutex_unlock(&m_SuspendMutexSend);
}
void checkSuspendSend()
{ // if suspended, suspend until resumed

    pthread_mutex_lock(&m_SuspendMutexSend);
    while (m_SuspendFlagSend != 0) pthread_cond_wait(&m_ResumeCondSend, &m_SuspendMutexSend);
    pthread_mutex_unlock(&m_SuspendMutexSend);
}

void suspendMeRecv()
{ // tell the thread to suspend
    pthread_mutex_lock(&m_SuspendMutexRecv);
    m_SuspendFlagRecv = 1;
    pthread_mutex_unlock(&m_SuspendMutexRecv);
}
void resumeMeRecv()
{ // tell the thread to resume
    pthread_mutex_lock(&m_SuspendMutexRecv);
    m_SuspendFlagRecv = 0;
    pthread_cond_broadcast(&m_ResumeCondRecv);
    pthread_mutex_unlock(&m_SuspendMutexRecv);
}
void checkSuspendRecv()
{ // if suspended, suspend until resumed

    pthread_mutex_lock(&m_SuspendMutexRecv);
    while (m_SuspendFlagRecv != 0) pthread_cond_wait(&m_ResumeCondRecv, &m_SuspendMutexRecv);
    pthread_mutex_unlock(&m_SuspendMutexRecv);
}