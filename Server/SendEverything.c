#include <sys/types.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <string.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <TCPEchoServer.h>


#define BROADCAST_IP "172.20.0.47"
#define SERVERPORT 4950

int SendEverything (int port){
    int fd;
    struct ifreq ifr;
    char *IP, *portsnd;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET; //get IP
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1); // IP attached eth0
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    IP = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
    strcat(IP, ":");
    sprintf(portsnd, "%d", port);
    strcat(IP, portsnd);
    Broadcast(BROADCAST_IP, SERVERPORT, IP);
    return 0;
}