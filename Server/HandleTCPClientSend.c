#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for recv() and send() */
#include <unistd.h>     /* for close() */
#include "amessage.pb-c.h"
#include "queue.h"
#include "queue_sync.h"

#define MAX_MSG_SIZE 1024

void DieWithError(char *errorMessage);  /* Error handling function */

void HandleTCPClientSend(int clntSocket) {
    AMessage msg = AMESSAGE__INIT; // AMessage
    void *buf;                     // Buffer to store serialized data
    unsigned len;                  // Length of serialized data
  //  int i;
    //np = head.cqh_last;
   // if(list_sync.count != list_sync.max_size)
    //for(i = 0; i< list_sync.max_size - list_sync.count; i++) np = np->entries.cqe_next;
  //  printf("pack: t=%d, len=%d\n", np->T, np->len);


    np = head.cqh_first;
    printf("pack: t=%d, len=%d\n", np->T, np->len);
    msg.a = np->T;
    msg.has_b = 1;
    msg.b = np->len;
    msg.c = np->str;
    /*msg.a = 1;
    msg.has_b = 1;
    msg.b = 2;
    msg.c = "as";*/
    if(np->len == 0) {
        printf("error message pack\n");
        close(clntSocket);    // Close client socket /
        return;
    }
    len = amessage__get_packed_size(&msg);
    buf = malloc(len);
    if(buf == NULL) DieWithError("malloc() failed");
    amessage__pack(&msg, buf);
    //Send data

    if (send(clntSocket, buf, len, 0) != len)
        DieWithError("send() failed");

    close(clntSocket);    // Close client socket /
   // np = np->entries.tqe_next;
    list_sync.count--;
    CIRCLEQ_REMOVE(&head, head.cqh_first, entries);
    printf("Send T=%d, Len=%d, STR=%.10s\n", msg.a, msg.b, msg.c);
}