//
// Created by nedelko on 8/29/16.
//

#ifndef KURS_QUEUE_SYNC_H
#define KURS_QUEUE_SYNC_H
#include <pthread.h>        /* for POSIX threads */

typedef struct{
    int count;
    int max_size;
} SYNC;
SYNC list_sync;

int m_SuspendFlagRecv;
int m_SuspendFlagSend;

pthread_mutex_t m_SuspendMutexSend;
pthread_cond_t m_ResumeCondSend;
pthread_mutex_t m_SuspendMutexRecv;
pthread_cond_t m_ResumeCondRecv;

void suspendMeSend();
void resumeMeSend();
void checkSuspendSend();
void suspendMeRecv();
void resumeMeRecv();
void checkSuspendRecv();
#endif //KURS_QUEUE_SYNC_H
