#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include "BroadcastReceiver.h"
#include "../Server/amessage.pb-c.h"

#define SERVERPORT 4951
#define MAX_MSG_SIZE 1024

void DieWithError(char *errorMessage);  /* Error handling function */

int main(int argc, char *argv[]) {
    int msgi = 1;
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    int bytesRcvd;   /* Bytes read in single recv()   and total bytes read */
    int time = 0;
    AMessage *msg;
    uint8_t buf[MAX_MSG_SIZE]; //Buffer to store serialized data
    printf("Start\n");
for(;;) {


    /* if ((argc < 3) || (argc > 4))    *//* Test for correct number of arguments *//*
    {
       fprintf(stderr, "Usage: %s <Server IP> <Echo Word> [<Echo Port>]\n",
               argv[0]);
       exit(1);
    }*/
    bytesRcvd = 1;
    char *p = BroadcastReceiver(SERVERPORT);
    servIP = strtok(p, ":");          /* First arg: server IP address (dotted quad) */
    echoServPort = atoi(strtok(NULL, "")); /* Use given port */
    printf("Received: IP %s Port %d\n", servIP, echoServPort);
    /*echoStringLen = generateLen();          *//* Determine input length *//*
    echoString = generateString(echoStringLen);         *//* Second arg: string to echo */

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port = htons(echoServPort); /* Server port */


    /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("connect() failed");
    // sleep(5);

    size_t recvMsgSize = 0;

    /* Receive data from the server */
    while (bytesRcvd > 0) {
        /* Receive up to the buffer size (minus 1 to leave space for
           a null terminator) bytes from the sender */
        if ((bytesRcvd = recv(sock, buf, MAX_MSG_SIZE, 0)) < 0)
            DieWithError("recv() failed or connection closed prematurely");
        recvMsgSize += bytesRcvd;   /* Keep tally of total bytes */
    }
    printf("meassage № %d\n", msgi);
    msgi++;
    printf("recived bytes %ld\n", recvMsgSize);
    msg = amessage__unpack(NULL, recvMsgSize, buf);
    if (msg == NULL) {
        fprintf(stderr, "error unpacking incoming message\n");
        close(sock);
        continue;
    }

    printf("Received: ");                /* Setup to print the echoed string */
    printf("T=%d Len=%d STR=%s", msg->a, msg->b, msg->c);      /* Print the echo buffer */
    printf("\n");    /* Print a final linefeed */
    time = (int) msg->a;
    sleep(time);
    close(sock);

}
    exit(0);
}
