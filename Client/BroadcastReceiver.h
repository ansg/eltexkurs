#ifndef KURS_BROADCASTRECEIVER_H
#define KURS_BROADCASTRECEIVER_H

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

char* BroadcastReceiver(int Port);

#endif //KURS_BROADCASTRECEIVER_H
