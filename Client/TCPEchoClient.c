#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <time.h>
#include "BroadcastReceiver.h"
#include "../Server/amessage.pb-c.h"

#define RCVBUFSIZE 32   /* Size of receive buffer */
#define SERVERPORT 4952
#define MAX_STR_LEN 1000
#define MAX_TIME 10

void DieWithError(char *errorMessage);  /* Error handling function */

/** @brief generate random lenght of string less MAX_STR_LEN
 * @return int len
 * */
int generateLen() {
    int len, stime;
    long ltime;
    /* получает текущее календарное время */
    ltime = time(NULL);
    stime = (unsigned) ltime / 2;
    srand(stime);
    len = rand() % MAX_STR_LEN;
    return len;
}

char *generateString(unsigned int len) {
    char *str;
    int j = 0;
    str = malloc(len);
    for (int i = 0; i < len; ++i) {
        str[i] = 'a' + j;
        j++;
        if (j > 25) j = 0;
    }
    return str;
}

int randT() {
    int i, stime;
    long ltime;
    /* получает текущее календарное время */
    ltime = time(NULL);
    stime = (unsigned) ltime / 2;
    srand(stime);
    return rand() % MAX_TIME;
}



int main(int argc, char *argv[]) {
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString;                /* String to send to echo server */
    char echoBuffer[RCVBUFSIZE];     /* Buffer for echo string */
    unsigned int echoStringLen;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
                                        and total bytes read */
    char *p;
    AMessage msg = AMESSAGE__INIT; // AMessage
    void *buf;                     // Buffer to store serialized data
    unsigned len;                  // Length of serialized data
    for(;;) {


        /* if ((argc < 3) || (argc > 4))    *//* Test for correct number of arguments *//*
    {
       fprintf(stderr, "Usage: %s <Server IP> <Echo Word> [<Echo Port>]\n",
               argv[0]);
       exit(1);
    }*/

        p = BroadcastReceiver(SERVERPORT);
        servIP = strtok(p, ":");          /* First arg: server IP address (dotted quad) */
        echoServPort = atoi(strtok(NULL, "")); /* Use given port */

        echoStringLen = generateLen();          /* Determine input length */
        echoString = generateString(echoStringLen);         /* Second arg: string to echo */

        /* Create a reliable, stream socket using TCP */
        if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");
        printf("Server: IP %s Port %d\n", servIP, echoServPort);
        /* Construct the server address structure */
        memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
        echoServAddr.sin_family = AF_INET;             /* Internet address family */
        echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
        echoServAddr.sin_port = htons(echoServPort); /* Server port */


        // sleep(5);

        /* Establish the connection to the echo server */



        if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
            DieWithError("connect() failed");

        msg.a = randT();
        msg.has_b = 1;
        msg.b = echoStringLen;
        msg.c = echoString;
        printf("send: T=%d, Len=%d, STR=%.10s", msg.a, msg.b, msg.c);
        len = amessage__get_packed_size(&msg);
        buf = malloc(len);
        amessage__pack(&msg, buf);

        /* Send the string to the server */
        if (send(sock, buf, len, 0) != len)
            DieWithError("send() sent a different number of bytes than expected");
        free(buf); // Free the allocated serialized buffer
        close(sock);
        sleep(msg.a);
    }
    exit(0);
}